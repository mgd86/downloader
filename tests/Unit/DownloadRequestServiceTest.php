<?php
namespace Tests\Unit;

use App\Exceptions\DownloadRequestValidationException;
use App\Jobs\ProcessDownloadRequest;
use App\Models\DownloadedFile;
use App\Models\DownloadRequest;
use App\Services\DownloadRequestService;
use App\Services\FileDownloadService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use Mockery;
use Exception;

class DownloadRequestServiceTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    /**
     * @var DownloadRequestService
     */
    private $downloadRequestService;

    /**
     * @var Mockery\MockInterface
     */
    private $fileDownloadServiceMock;

    protected function setUp()
    {
        parent::setUp();

        Storage::fake();

        $this->fileDownloadServiceMock = Mockery::mock(FileDownloadService::class);
        $this->downloadRequestService = new DownloadRequestService($this->fileDownloadServiceMock);
    }

    public function testGetAllDownloadRequests()
    {
        /** @var Collection $downloadRequests */
        $downloadRequests = factory(DownloadRequest::class, 3)->create();
        $downloadRequests->push(factory(DownloadRequest::class)->states('complete')->create());
        $downloadRequests->push(factory(DownloadRequest::class)->states('error')->create());
        $downloadRequests->push(factory(DownloadRequest::class)->states('downloading')->create());

        $result = $this->downloadRequestService->getAllDownloadRequests();

        $this->assertCount(0, $downloadRequests->diff($result));
    }

    public function testAddNewDownloadRequest()
    {
        Bus::fake();

        $this->assertEquals(0, DownloadRequest::count());
        $url = $this->faker->url;
        $downloadRequest = $this->downloadRequestService->addNewDownloadRequest($url);
        $this->assertEquals(1, DownloadRequest::count());

        Bus::assertDispatched(ProcessDownloadRequest::class, function ($job) use ($downloadRequest) {
            return $job->downloadRequest->id === $downloadRequest->id;
        });
    }

    public function testAddNewDownloadShouldValidateData()
    {
        Bus::fake();

        $this->assertEquals(0, DownloadRequest::count());
        $url = $this->faker->randomAscii;

        try {
            $this->downloadRequestService->addNewDownloadRequest($url);
            $this->assertTrue(false, 'Validation exception should be thrown');
        } catch (Exception $e) {
            $this->assertEquals(DownloadRequestValidationException::class, get_class($e));
        }

        $this->assertEquals(0, DownloadRequest::count());

        Bus::assertNotDispatched(ProcessDownloadRequest::class);
    }

    public function testProcessDownloadRequest()
    {
        $downloadRequest = factory(DownloadRequest::class)->create();
        $file = factory(DownloadedFile::class)->make();

        $this->fileDownloadServiceMock
             ->shouldReceive('downloadFile')
             ->once()
             ->with($downloadRequest->url)
             ->andReturn($file);
        $this->downloadRequestService->processDownloadRequest($downloadRequest);

        $this->assertEquals(DownloadRequest::STATUS_COMPLETE, $downloadRequest->status);
        $this->assertSame($file, $downloadRequest->file);
    }

    public function testProcessDownloadRequestShouldHandleErrorDownload()
    {
        $downloadRequest = factory(DownloadRequest::class)->create();
        $exceptionMessage = 'File could not be downloaded.';

        $this->fileDownloadServiceMock
            ->shouldReceive('downloadFile')
            ->once()
            ->with($downloadRequest->url)
            ->andThrow(new Exception($exceptionMessage));
        $this->downloadRequestService->processDownloadRequest($downloadRequest);

        $this->assertEquals(DownloadRequest::STATUS_ERROR, $downloadRequest->status);
        $this->assertSame($exceptionMessage, $downloadRequest->error_message);
    }

    protected function tearDown()
    {
        //Call this to delete files from testing storage
        Storage::fake();

        parent::tearDown();
    }
}
