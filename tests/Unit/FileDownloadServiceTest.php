<?php
namespace Tests\Unit;

use App\Models\DownloadedFile;
use App\Services\FileDownloadService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class FileDownloadServiceTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    /**
     * @var FileDownloadService
     */
    private $fileDownloadService;


    protected function setUp()
    {
        parent::setUp();

        Storage::fake();

        $this->fileDownloadService = new FileDownloadService();
    }

    public function testDownloadFile()
    {
        $faker = $this->faker;
        $file = UploadedFile::fake()->create($faker->unique()->word . '.' . $faker->fileExtension, 10);

        $this->assertEquals(0, DownloadedFile::count());
        $downloadedFile = $this->fileDownloadService->downloadFile($file->getPath());

        $this->assertNotNull($downloadedFile);
        $this->assertEquals(1, DownloadedFile::count());
    }

    protected function tearDown()
    {
        Storage::fake();

        parent::tearDown();
    }
}
