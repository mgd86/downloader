<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DownloadedFile extends Model
{
    protected $table = 'downloaded_files';

    protected $fillable = ['path', 'original_filename'];

    public function request()
    {
        return $this->hasMany(DownloadRequest::class);
    }

    public function getFileNameAttribute(): string
    {
        return $this->original_filename !== null ? $this->original_filename : basename($this->path);
    }
}
