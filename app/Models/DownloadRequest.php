<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DownloadRequest extends Model
{
    const STATUS_PENDING        = 'pending';
    const STATUS_DOWNLOADING    = 'downloading';
    const STATUS_COMPLETE       = 'complete';
    const STATUS_ERROR          = 'error';

    protected $table = 'download_requests';

    protected $fillable = ['url'];

    protected $visible = ['id', 'status', 'url', 'error_message', 'file'];

    protected $attributes = [
        'status' => self::STATUS_PENDING,
    ];

    public function file()
    {
        return $this->belongsTo(DownloadedFile::class);
    }
}
