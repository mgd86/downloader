<?php
namespace App\Services;

use App\Exceptions\DownloadRequestValidationException;
use App\Jobs\ProcessDownloadRequest;
use App\Models\DownloadRequest;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Exception;

class DownloadRequestService
{
    /** @var FileDownloadService */
    private $fileDownloadService;

    public function __construct(FileDownloadService $fileDownloadService)
    {
        $this->fileDownloadService = $fileDownloadService;
    }

    public function getAllDownloadRequests(): Collection
    {
        return DownloadRequest::with('file')->orderBy('id','desc')->get();
    }

    /**
     * @throws DownloadRequestValidationException
     */
    public function addNewDownloadRequest(string $url): DownloadRequest
    {
        $data = ['url' => $url];
        $this->validateDownloadRequestData($data);

        $downloadRequest = DownloadRequest::create($data);

        DB::transaction(function() use ($downloadRequest) {
            $downloadRequest->save();
            ProcessDownloadRequest::dispatch($downloadRequest);
        });

        return $downloadRequest;
    }

    public function processDownloadRequest(DownloadRequest $downloadRequest)
    {
        $downloadRequest->status = DownloadRequest::STATUS_DOWNLOADING;
        $downloadRequest->save();

        try {
            $file = $this->fileDownloadService->downloadFile($downloadRequest->url);
            $downloadRequest->file()->associate($file);
            $downloadRequest->status = DownloadRequest::STATUS_COMPLETE;
        } catch (Exception $e) {
            $downloadRequest->status = DownloadRequest::STATUS_ERROR;
            $downloadRequest->error_message = $e->getMessage();
        }

        $downloadRequest->save();
    }

    /**
     * @throws DownloadRequestValidationException
     */
    private function validateDownloadRequestData(array $downloadRequestData): bool
    {
        $validator = Validator::make($downloadRequestData, [
            'url' => 'required|url|max:500',
        ]);

        if ($validator->fails()) {
            $exception = new DownloadRequestValidationException();
            $exception->setErrors($validator->errors()->all());
            throw $exception;
        }

        return true;
    }
}
