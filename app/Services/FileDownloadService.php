<?php
namespace App\Services;

use App\Models\DownloadedFile;
use Illuminate\Support\Facades\Storage;

class FileDownloadService
{
    public function downloadFile(string $url): DownloadedFile
    {
        $filePath = uniqid();

        Storage::disk('local')->putStream($filePath, fopen($url, 'r'));
        $originalFilename = basename(parse_url($url, PHP_URL_PATH));
        $file = DownloadedFile::create([
            'path' => $filePath,
            'original_filename' => $originalFilename,
        ]);

        return $file;
    }
}
