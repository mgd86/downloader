<?php
namespace App\Console\Commands;

use App\Exceptions\DownloadRequestValidationException;
use App\Services\DownloadRequestService;
use Illuminate\Console\Command;

class DownloadRequestAddCommand extends Command
{
    protected $signature = 'download-request:add {url : Url of the file to download}';

    protected $description = 'Add new request for file download.';

    /**
     * @var DownloadRequestService
     */
    private $downloadRequestService;

    public function __construct(DownloadRequestService $downloadRequestService)
    {
        parent::__construct();

        $this->downloadRequestService = $downloadRequestService;
    }

    public function handle()
    {
        $url = (string)$this->argument('url');
        try {
            $this->downloadRequestService->addNewDownloadRequest($url);
            $this->info('Download request was successfully added');
        } catch (DownloadRequestValidationException $e) {
            $this->error(json_encode($e->getErrors()));
        }
    }
}
