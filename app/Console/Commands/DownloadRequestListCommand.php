<?php
namespace App\Console\Commands;

use App\Http\Resources\DownloadRequestResource;
use App\Services\DownloadRequestService;
use Illuminate\Console\Command;

class DownloadRequestListCommand extends Command
{
    protected $signature = 'download-request:list';

    protected $description = 'List of the existing download requests in database.';

    /**
     * @var DownloadRequestService
     */
    private $downloadRequestService;

    public function __construct(DownloadRequestService $downloadRequestService)
    {
        parent::__construct();

        $this->downloadRequestService = $downloadRequestService;
    }

    public function handle()
    {
        $requests = $this->downloadRequestService->getAllDownloadRequests();

        foreach ($requests as $request) {
            $this->line(
                json_encode(new DownloadRequestResource($request), JSON_UNESCAPED_SLASHES)
            );
        }
    }
}
