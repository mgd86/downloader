<?php
namespace App\Jobs;

use App\Models\DownloadRequest;
use App\Services\DownloadRequestService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProcessDownloadRequest implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var DownloadRequest
     */
    public $downloadRequest;

    public function __construct(DownloadRequest $downloadRequest)
    {
        $this->downloadRequest = $downloadRequest;
    }

    public function handle(DownloadRequestService $downloadRequestService)
    {
        $downloadRequestService->processDownloadRequest($this->downloadRequest);
    }
}
