<?php
namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class DownloadRequestResource extends Resource
{
    /**
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $resourceArray = parent::toArray($request);
        if (array_key_exists('file', $resourceArray) && is_array($resourceArray['file'])) {
            $resourceArray['file'] = route('file.download', ['path' => $resourceArray['file']['path']]);
        }
        return $resourceArray;
    }
}
