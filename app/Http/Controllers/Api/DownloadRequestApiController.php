<?php
namespace App\Http\Controllers\Api;

use App\Exceptions\DownloadRequestValidationException;
use App\Http\Controllers\Controller;
use App\Http\Resources\DownloadRequestResource;
use App\Services\DownloadRequestService;
use Illuminate\Http\Request;

class DownloadRequestApiController extends Controller
{
    /**
     * @var DownloadRequestService
     */
    private $downloadRequestService;

    public function __construct(DownloadRequestService $downloadRequestService)
    {
        $this->downloadRequestService = $downloadRequestService;
    }

    public function index()
    {
        return DownloadRequestResource::collection($this->downloadRequestService->getAllDownloadRequests());
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $downloadRequest = $this->downloadRequestService->addNewDownloadRequest($request->post('url', ''));
            return new DownloadRequestResource($downloadRequest);
        } catch (DownloadRequestValidationException $e) {
            return response()->json([
                'errors' => $e->getErrors()
            ], 400);
        }
    }
}
