<?php
namespace App\Http\Controllers;

use App\Exceptions\DownloadRequestValidationException;
use App\Services\DownloadRequestService;
use Illuminate\Http\Request;

class DownloadRequestController extends Controller
{
    /**
     * @var DownloadRequestService
     */
    private $downloadRequestService;

    public function __construct(DownloadRequestService $downloadRequestService)
    {
        $this->downloadRequestService = $downloadRequestService;
    }

    public function index()
    {
        return view('index', ['requests' => $this->downloadRequestService->getAllDownloadRequests()]);
    }

    public function store(Request $request)
    {
        try {
            $this->downloadRequestService->addNewDownloadRequest($request->post('url', ''));
        } catch (DownloadRequestValidationException $e) {
            return redirect()
                    ->route('download_request.list')
                    ->withErrors($e->getErrors())->withInput();
        }

        return redirect()->route('download_request.list');
    }
}
