<?php
namespace App\Http\Controllers;

use App\Models\DownloadedFile;
use Illuminate\Support\Facades\Storage;

class DownloadedFileController extends Controller
{
    public function download(DownloadedFile $file)
    {
        return Storage::disk('local')->download($file->path, $file->file_name);
    }
}
