<?php
namespace App\Exceptions;

use Exception;

class DownloadRequestValidationException extends Exception
{
    private $errors = [];

    public function setErrors(array $errors)
    {
        $this->errors = $errors;
    }

    public function getErrors(): array
    {
        return $this->errors;
    }
}
