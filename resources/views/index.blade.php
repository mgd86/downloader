<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Downloader</title>
        <link href="{{ asset('css/app.css') }}" rel=stylesheet>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 margin">
                    <form method="POST" action=" {{ route('download_request.store') }}">
                        {{ csrf_field() }}
                        <div class="form-group col-xs-5">
                            <input name="url" value="{{ old('url') }}" type="text" class="form-control" placeholder="Enter Url">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        </div>
                        <button type="submit" class="btn btn-primary">Download</button>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Status</th>
                                <th>Url</th>
                                <th>File</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($requests as $request)
                            <tr>
                                <td>{{ $request->id }}</td>
                                <td>{{ $request->status }}</td>
                                <td>{{ $request->url }}</td>
                                <td>
                                    @if ($request->file !== null)
                                        <a href="{{ route('file.download', ['path' => $request->file->path]) }}">
                                            {{ $request->file->file_name }}
                                        </a>
                                    @else
                                        -
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>
