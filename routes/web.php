<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DownloadRequestController@index')->name('download_request.list');
Route::post('/', 'DownloadRequestController@store')->name('download_request.store');
Route::get('/download/{path}', 'DownloadedFileController@download')->name('file.download');
