<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDownloadedFilesTable extends Migration
{
    public function up()
    {
        Schema::create('downloaded_files', function (Blueprint $table) {
            $table->increments('id');
            $table->string('path')->unique();
            $table->string('original_filename')->nullable();
            $table->timestamps();
        });

        Schema::table('download_requests', function (Blueprint $table) {
            $table->integer('file_id')->nullable();
        });
    }

    public function down()
    {
        Schema::table('download_requests', function (Blueprint $table) {
            $table->dropColumn('file_id');
        });
        Schema::dropIfExists('downloaded_files');
    }
}
