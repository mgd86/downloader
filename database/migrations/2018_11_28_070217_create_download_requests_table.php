<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDownloadRequestsTable extends Migration
{
    public function up()
    {
        Schema::create('download_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('status');
            $table->string('url');
            $table->string('error_message')->nullable();
            $table->timestamps();

            $table->index('status');
        });
    }

    public function down()
    {
        Schema::dropIfExists('download_requests');
    }
}
