<?php
use Faker\Generator as Faker;
use App\Models\DownloadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;

$factory->define(DownloadedFile::class, function (Faker $faker) {
    $fileName = $faker->unique()->word . '.' . $faker->fileExtension;
    $file = UploadedFile::fake()->create($fileName);
    $path = Storage::put('', $file);
    return [
        'path' => $path,
        'original_filename' => $fileName,
    ];
});

