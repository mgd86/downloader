<?php
use Faker\Generator as Faker;
use App\Models\DownloadRequest;
use App\Models\DownloadedFile;

$factory->define(DownloadRequest::class, function (Faker $faker) {
    return [
        'status' => DownloadRequest::STATUS_PENDING,
        'url' => $faker->url,
    ];
});

$factory->state(DownloadRequest::class, 'complete', function (Faker $faker) {
    return [
        'status'    => DownloadRequest::STATUS_COMPLETE,
        'file_id'   => function () {
            return factory(DownloadedFile::class)->create()->id;
        },
    ];
});

$factory->state(DownloadRequest::class, 'error', function (Faker $faker) {
    return [
        'status'    => DownloadRequest::STATUS_ERROR,
        'error_message' => $faker->text,
    ];
});

$factory->state(DownloadRequest::class, 'downloading', function (Faker $faker) {
    return [
        'status'    => DownloadRequest::STATUS_DOWNLOADING,
    ];
});
