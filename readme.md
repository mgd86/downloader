# Downloader Test Project

Main goal of the project is give an ability for users to request file download through web interface, api and console command

## Installation

#####Code
- Install dependencies: `composer install`
- Set env values: `cp .env.example .env`. You can modify them if required.
- Generate app secure key: `artisan key:generate`

####Database
For dev purposes I've used sqlite database, if you going to do the same you need to do additional steps:
- Make sure that you have sqlite php module, and install it if you haven't `sudo apt install php7.0-sqlite`
- Create work and test database files: `touch ./database/database.sqlite` `touch ./database/database_test.sqlite`
- Make sure that database files are readable and writable for your web user

Run migrations: `artisan migrate`

##Main features

Application has three ways to interact with user

#####Web interface

It has one page where you can see list of requested files for download. You can add new request by passing url into the 
field on the top of the page and hit 'Download' button.

Each request in the list has status column and when it became `complete` you will see link to the requested file.

#####API 

Api has two endpoints

|Method|URI|Parameters|Description|
|---|---|---|---|
|GET|/api/download-request/|-|Shows list of existed download requests|
|POST|/api/download-request/|url: string|Adds new download request to the queue|

#####Console

To interact with console you can use next commands

|Command|Description|
|---|---|
|download-request:list|Displays list of existed download requests|
|download-request:add {url}|Adds new download request to the queue|

## Questions and Issues

If you have any question about this test project feel free to contact me [andrew.dudnik@gmail.com](mailto:andrew.dudnik@gmail.com)


